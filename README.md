# Phantom Generator #

This MATLAB script will generate an arbitrary number of ellipsoids in a 3D matrix and save it as a 3D TIFF-file.

### Overview ###

Cell-resembling ellipsoid structures are drawn into a 3D matrix. The ellipsoids have certain properties, these include:

* Size (randomized if needed)
* Rotation (randomized if needed)
* Pixel value

The base matrix also has its own set of properties, namely:

* x & y dimension
* z dimension
* Pixel size (x & y combined, z separate)

The output of this script will be an image file and an accompanying spreadsheet with the location and sizes of each drawn cell.

### Getting started ###

Required MATLAB version: in theory at least R2019a (due to the usage of *writematrix()*), but development and testing was done in R2020b

* Clone or download the repository
* Open main.m and set values to the desired specification
* Making sure the folder structure is intact, run the code in main.m
* Output picture will be saved as "image.tif" in the root folder (same as main.m)
* Output spreadsheet will be saved as "eIndex.xlsx" in the root folder

### Upcoming features ###

* (Adjustable) background noise/glow
* Blur

### Contact ###

tom (at) tleeuwen (dot) nl  
Uhlen group, Dept. Of Medical Biochemistry and Biophysics  
Molecular Neurobiology Unit  
Karolinska Institutet  
Stockholm, Sweden  