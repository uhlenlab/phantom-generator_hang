function factor = calculateScaledNormal(variation)
%CALCULATESCALEDNORMAL Calculates a random number on a scaled normal distribution
%   As adapted from Andrei Bobrov's solution on
%   https://se.mathworks.com/matlabcentral/answers/10366-using-randn-within-an-interval#answer_14265
%   https://web.archive.org/web/20210406131033/https://se.mathworks.com/matlabcentral/answers/10366-using-randn-within-an-interval

x = 512;

variationDec = variation/100;
itl = [-(variationDec) (variationDec)]; % interval
list = randn([x 1]);
listScaled = itl(1)+diff(itl)*(list-min(list(:)))/(max(list(:))-min(list(:)));
factor = listScaled(randi(x));
end
