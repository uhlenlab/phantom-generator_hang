function answer = calculateOverlap(Xcenter,Ycenter,Zcenter,Xmax,Ymax,Zmax,t)
%CALCULATEOVERLAP Takes two cells and compares their centers
%   This script compares one cell that is going to be drawn and a batch of
%   cells that have been drawn already. We take the centers and maximum
%   dimensions and draw cubes around that, and determine the overlap
%   between those cubes.

if height(t) == 0 % For when the batches start anew and t is empty
    answer = true;
    return;
end

limit = 25; % Max overlap, in percentage of both ellipsoids
cubedI = Xmax*Ymax*Zmax;

for k = 1:height(t) % For all items loaded into memory
    
    cubedK = t(k,5)*t(k,6)*t(k,7);
    
    x = abs(t(k,2) - Xcenter); % Determine distances between midpoints
    y = abs(t(k,3) - Ycenter);
    z = abs(t(k,4) - Zcenter);
    overlapX = (t(k,5)/2 + Xmax/2) - x; % Deduce overlap from displacements
    overlapY = (t(k,6)/2 + Ymax/2) - y;
    overlapZ = (t(k,7)/2 + Zmax/2) - z;
    overlap = overlapX * overlapY * overlapZ;
    ratio = overlap/(cubedI+cubedK); % Overlap as a fraction of both items
    
    if (overlapX>0) && (overlapY>0) && (overlapZ>0) && (ratio>(limit/100))
        answer = false;
        return; % If found to be too much, the result is returned directly
    else
        answer = true;
    end
end
end