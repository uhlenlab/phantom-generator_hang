function [Xcenter,Ycenter,Zcenter] = ellipsoidCenter(Xmax,Ymax,Zmax,xy,z,i,T,b)
%ELLIPSOIDCENTER Returns valid X, Y, Z coordinates
%   Randomly assigns X Y Z coordinates and passes them to checkOverlap(),
%   which returns whether they are overlapping too much with existing cells
%   or not.

check = false;

while check == false
    
    % Random placement
    Xcenter = round(Xmax/2) + randi(round(xy - Xmax));
    Ycenter = round(Ymax/2) + randi(round(xy - Ymax));
    Zcenter = round(Zmax/2) + randi(round(z - Zmax));
    
    % Calculate overlap
    overlap = checkOverlap(Xcenter,Ycenter,Zcenter,Xmax,Ymax,Zmax,i,T,b);
    
    % If the coordinates are within boundaries of another object
    if overlap
        check = true;
        eIndex = [i,Xcenter,Ycenter,Zcenter,Xmax,Ymax,Zmax];
        addTsmall(eIndex);
    else
        disp('Retry fitting')
    end
end
end

