function answer = checkOverlap(Xcenter,Ycenter,Zcenter,Xmax,Ymax,Zmax,i,T,batchNumber)
%CHECKOVERLAP Summary of this function goes here
%   Detailed explanation goes here

% Loading the table in batches should speed up the calculations
batches = floor((i-1)/batchNumber);

if i>1 % If we are looking at more than 1 cell
    
    t = getTsmall;
    
    % The actual check can be found here
    answer = calculateOverlap(Xcenter,Ycenter,Zcenter,Xmax,Ymax,Zmax,t);
    
    % Loop through the batches until a false result is found
    if (batches > 0) && (answer == true)
        for j = 1:batches
            if answer == true
                
                lower = batchNumber*(j-1) + 1; % Lower boundary
                upper = batchNumber*j; % Upper boundary
                
                t = table2array(gather(T(lower:upper,:)));
                
                answer = calculateOverlap(Xcenter,Ycenter,Zcenter,Xmax,Ymax,Zmax,t);
            end
        end
    end
else
    answer = true;
end
end

