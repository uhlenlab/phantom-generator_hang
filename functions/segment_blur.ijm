// Import all real and all fake images as virtual stacks
run("Image Sequence...", "open="+filePath+" number=1 starting=1 file=real convert sort use");
rename("images_real");
getDimensions(width, height, channels, slices, frames);

run("Image Sequence...", "open="+filePath+" number=1 starting=1 file=fake convert sort use");
rename("images_fake");

// Crop fake images to correspond to real image size
makeRectangle(0, 0, width, height);
run("Crop");

// Create segmented image based on fake & real data
imageCalculator("AND create stack", "images_real","images_fake");

// Duplicate & blur segmented images
selectWindow("Result of images_real");
run("Duplicate...", "duplicate");
run("Gaussian Blur 3D...", "x=5 y=5 z=1");

// Create random noise background
newImage("bg", "8-bit noise", width, height, slices);
run("Gaussian Blur 3D...", "x=5 y=5 z=1");
run("Subtract...", "value=80 stack");

// Compile everything into one big beautiful stack
imageCalculator("Max create stack", "bg","Result of images_real-1");
selectWindow("Result of bg");
imageCalculator("Max create stack", "Result of bg","Result of images_real");
selectWindow("Result of Result of bg"); // Such a beautiful name too

// Save


// Close
close("*");