function matrix = drawEllipsoids(e,matrix,Pxy,Pz,val)
%DRAWELLIPSOIDS Draw the ellipsoids into a matrix
%   An ellipsoid is drawn from the outside in.
r = e.rUnique;

for j = r:-1:1
    
    Xsr = round((e.X*j)/2); % Scale the base sphere coordinates to the radius, then divide by 2 to actually end up with the radius (and not diameter)
    Ysr = round((e.Y*j)/2); % We need to round these coordinates to make them compatible with matrix coordinates
    Zsr = round(((e.Z*j)/2)*Pxy/Pz); % Scale to the Z axis
    
    Mlength = length(Xsr); % We need this to know the number of iterations that we'll require in the next step
    
    for sidesA = 1:Mlength % Because the coordinates for the sphere are in a 2D matrix, we need to iterate over both dimensions
        
        for sidesB = 1:Mlength % The second dimension
            % Then select the corresponding point on each of the
            % coordinate matrices and write to the empty matrix
            matrix((e.Ycenter+Ysr(sidesA,sidesB)),(e.Xcenter+Xsr(sidesA,sidesB)),(e.Zcenter+Zsr(sidesA,sidesB))) = val;
        end
    end
end
end

