classdef ellipsoidVolume
    %ELLIPSOIDVOLUME Artificial cell
    %   Takes dimension constraints and info on preceding ellipsoids, from
    %   there it creates a suitable 3D center coordinate and cellular
    %   dimensions.
    
    properties
        X {mustBeNumeric}
        Y {mustBeNumeric}
        Z {mustBeNumeric}
        Xmax {mustBeNumeric}
        Ymax {mustBeNumeric}
        Zmax {mustBeNumeric}
        rUnique {mustBeNumeric}
        Xcenter {mustBeNumeric}
        Ycenter {mustBeNumeric}
        Zcenter {mustBeNumeric}
    end
    
    methods
        function obj = ellipsoidVolume(radius,rotation,random,variation,xy,z,Pxy,Pz,i,T,b)
            %ELLIPSOIDVOLUME Construct an instance of this class
            %   Detailed explanation goes here
            if random % if random is true
                obj.rUnique = round(radius * (1 + calculateScaledNormal(variation)));
            else
                obj.rUnique = radius;
            end
            
            n = (radius+random)*10;
            
            [obj.X,obj.Y,obj.Z] = ellipsoid(0,0,0,1.5,1,1,n); % Centers for x y z are all 0
            
            if rotation % if rotation is true
                eObj = surf(obj.X,obj.Y,obj.Z); % Save surfaces
                
                rotate(eObj,[1 0 0],rand()*180); % Rotate
                rotate(eObj,[0 1 0],rand()*180);
                rotate(eObj,[0 0 1],rand()*180);
                
                obj.X = eObj.XData; % Save raw rotated data
                obj.Y = eObj.YData;
                obj.Z = eObj.ZData;
                
                obj.Xmax = round(obj.rUnique*max(max(obj.X))); % Determine max dimensions of the ellipsoid.
                obj.Ymax = round(obj.rUnique*max(max(obj.Y))); % Because X/Y/Z are 2D arrays, we need to take the
                obj.Zmax = round((obj.rUnique*max(max(obj.Z)))*Pxy/Pz); % maximum of the maximum
            else
                obj.Xmax = round(obj.rUnique*1.5);
                obj.Ymax = round(obj.rUnique);
                obj.Zmax = round(obj.rUnique*Pxy/Pz);
            end
            
            [obj.Xcenter,obj.Ycenter,obj.Zcenter] = ellipsoidCenter(obj.Xmax,obj.Ymax,obj.Zmax,xy,z,i,T,b); % Determine centers
        end
    end
end

