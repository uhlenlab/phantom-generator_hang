% 3D Ellipsoid drawer
%
% In this script, we first create a 3D environment, in which we then draw
% 3D objects. The 3D objects will be ellipsoids, in order to resemble cells
% as much as possible. Author: Tom van Leeuwen, Ulhén Group, Dept. of MBB,
% Karolinska Institutet, Stockholm, Sweden

% Clear and add functions
clear
addpath(genpath('functions'));
setTsmall([]);

%%%%%%%%%%%%%%%%%%%%%%
%%%%% SET VALUES %%%%%
%%%%%%%%%%%%%%%%%%%%%%

% First, set xy and z dimensions
xy = 1024;
z = 128;

% Set a pixel size in microns, each pixel of xy translates to Pxy number of
% microns in the final image. This will distort the shape of the objects
Pxy = 1;
Pz = 1;

% Set number of objects and their approximate radius in microns
Nellipsoids = 2000;
rMicron = 10;

% Set variation in size to true or false. Variation will give the sizes a
% normal distribution
rVar = true;

% How much size variation is allowed, in percent of microns. 
% N.B.: This variation works in both positive and negative! (E.g. 
% rVarRange = 25 means a range of -25% to +25%)
rVarRange = 10; 

% Allow the volumes to be rotated. Rotation will happen in any direction,
% with any amount of degrees. Can be true or false.
rotate = true;

% Value for the inside of the sphere
val = 255;

%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% RUN GENERATOR %%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%

r = round(rMicron/Pxy); % Convert the micron radius of the sphere to pixels

possibleA = (r*r*(1.5*r))*Nellipsoids*0.75; % Check fill possibility
possibleB = xy*xy*z;
if possibleA > possibleB
    disp('Not enough space to fit all ellipsoids. Exiting script.')
    return
end

M = []; % Creating a matrix
M(:,:,z) = zeros(xy); % Fill matrix M with xy planes filled with zeroes

if isfile('eIndex.xlsx') % Create an empty file eIndex.xlsx for storage
    delete('eIndex.xlsx');
end
varnames = {'n','Xcenter','Ycenter','Zcenter','Xmax','Ymax','Zmax'};
writecell(varnames,'eIndex.xlsx'); % Variables
ds = datastore('eIndex.xlsx'); % Create a datastore
T = tall(ds);

batchNumber = 1000;

for i = 1:Nellipsoids % For N of ellipsoids
    
    disp(i)
    disp('Progress:')
    disp((i/Nellipsoids)*100)
    
    e = ellipsoidVolume(r,rotate,rVar,rVarRange,xy,z,Pxy,Pz,i,T,batchNumber);
    
    M = drawEllipsoids(e,M,Pxy,Pz,val);
    
    if mod(i,batchNumber) == 0
        t = getTsmall;
        writematrix(t,'eIndex.xlsx','WriteMode','append');
        setTsmall([]);
        T = tall(ds); % Using the datastore is only practical when there is
                      % stored data
    elseif i == Nellipsoids
        t = getTsmall;
        writematrix(t,'eIndex.xlsx','WriteMode','append');
        setTsmall([]);
    end
end

%%%%%%%%%%%%%%%%%%%%%%
%%%%% DRAW IMAGE %%%%%
%%%%%%%%%%%%%%%%%%%%%%

% Write to a 3D tiff file, coded in 8-bit unsigned integers (uint8)
Mimage = cast(M,'uint8');
disp('Writing image...')
imwrite(Mimage(:,:,1),'image.tif'); % Write the first layer to a .tif file
for i = 2:z % Then, because we have a 3rd dimension, we need to append further layers to the newly written .tif file
    imwrite(Mimage(:,:,i),'image.tif','WriteMode','append');
end
disp('Finished!')